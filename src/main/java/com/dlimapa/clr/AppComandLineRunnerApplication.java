package com.dlimapa.clr;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AppComandLineRunnerApplication {

	public static void main(String[] args) {
		System.out.println("### entrando main");
		SpringApplication.run(AppComandLineRunnerApplication.class, args);
		System.out.println("### saindo main");
	}
	
	@Bean
	public CommandLineRunner metodoCLRUm() {
		return 	args -> {
			System.out.println("### método metodoCLRUm");
		};
	}

	@Bean
	public CommandLineRunner metodoCLRDois() {
		return 	args -> {
			System.out.println("### método metodoCLRDois");
		};
	}
}
