package com.dlimapa.clr;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

@Component
public class SegundoCommadLineClasse implements CommandLineRunner, Ordered {

	@Override
	public int getOrder() {
		return 2;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("### método da classe " + this.getClass().getName());
	}

}
