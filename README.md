# CommandLineRunner

É executado no momento da carga da aplicação SpringBoot, ou seja, após/durante o método run da classe SpringApplication.

Estrutura do método:

```java
@Bean
public CommandLineRunner commadnLineRunner() {

    return args -> {

        //comandos a serem executados

    };

}
```

Pode-se ter vários métodos na classe anotada com @SpringBootApplication. Eles serão execitados na ordem que se encontra na classe.

Podemos implementar as interfaces CommandLineRunner, Ordered, definindo a ordem de chamada e o corpo do que será executado.

```java
package com.dlimapa.clr;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

@Component

public class PrimeiroCommadLineClasse implements CommandLineRunner, Ordered {
	@Override
	public int getOrder() {
		return 1;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("### método da classe " + this.getClass().getName());
	}
}
```